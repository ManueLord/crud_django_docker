"""crud_dj URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path

from crud_dj_app.views import crudList, crudDetails, crudCreate, crudUpdate, crudDelete

urlpatterns = [
    path('admin/', admin.site.urls),
    path('index/', crudList.as_view(), name='index'),
    path('index/look/<int:pk>', crudDetails.as_view(), name='look'), 
    path('index/create/', crudCreate.as_view(template_name = 'crud_dj_app/create.html'), name='create'),
    path('index/update/<int:pk>', crudUpdate.as_view(template_name = 'crud_dj_app/update.html'), name='update'),
    path('index/delete/<int:pk>', crudDelete.as_view(), name='delete'),
]
