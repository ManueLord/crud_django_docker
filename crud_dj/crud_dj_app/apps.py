from django.apps import AppConfig


class CrudDjAppConfig(AppConfig):
    name = 'crud_dj_app'
