from django.shortcuts import render

from django.views.generic import ListView, DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from .models import Usuarios
from django.urls import reverse
from django.contrib import messages 
from django.contrib.messages.views import SuccessMessageMixin

# Create your views here.
'''
class crudList(ListView):
    template_name = 'crud_dj_app/index.html'
    context_object_name = 'object_list'
    def get_queryset(self):
		return Usuarios.objects.all()
'''
class crudList(ListView):
    model = Usuarios
    template_name = 'crud_dj_app/index.html'

class crudDetails(DetailView):
    model = Usuarios
    template_name = 'crud_dj_app/look.html'

class crudCreate(SuccessMessageMixin, CreateView):
    model = Usuarios
    form = Usuarios
    fields = "__all__"
    success_messanger = 'Correct Create'
    def get_success_url(self):        
        return reverse('index')

class crudUpdate(UpdateView, SuccessMessageMixin):
    model = Usuarios
    form = Usuarios 
    fields = "__all__"
    success_messanger = 'Correct Update'
    def get_success_url(self):        
        return reverse('index')

class crudDelete(DeleteView, SuccessMessageMixin):
    model = Usuarios
    form = Usuarios 
    fields = "__all__"  
    def get_success_url(self): 
        success_message = 'Delete !' 
        messages.success (self.request, (success_message))       
        return reverse('index')
