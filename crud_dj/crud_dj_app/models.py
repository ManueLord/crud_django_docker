from django.db import models

# Create your models here.

class Usuarios(models.Model):
    name = models.CharField(max_length=30)
    year_old = models.IntegerField()
    tel = models.CharField(max_length=7)
    email= models.EmailField()
